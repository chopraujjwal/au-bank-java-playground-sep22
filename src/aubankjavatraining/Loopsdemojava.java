package aubankjavatraining;

import java.util.Scanner;

public class Loopsdemojava {
	public static void main(String[] args) {
	
		Scanner sc= new Scanner(System.in);
		System.out.println("enter name");
		String name= sc.next();
		System.out.println("enter city");
		String city= sc.next();
		System.out.println("enter age");
		int age= sc.nextInt();
		System.out.println("enter temp");
		int temp= sc.nextInt();
		
		System.out.println("hey "+name+" ,you are"+age+"years old");
		System.out.println("Currently you are in "+city);
		System.out.println("temperature there is "+temp+" degree celsius");
		sc.close();

  }
}
