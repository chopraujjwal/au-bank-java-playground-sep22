package aubankjavatraining;

public class SwitchCase {
	public static void main(String[] args) {
		String role= "hello";
		switch (role) {
		case "Developer":
			System.out.println("Password is dev@123");
			break;
		case "Admin":
			System.out.println("Password is Admin@123");
			break;
		case "tester":
			System.out.println("password is tester@123");
			break;

		default:
			System.out.println("password is guest@123");
			break;
		}
	}

}
